/*
Devoir #1
Fichier : main.cpp
Calcul de trajectoire d'un projectile sur la Lune ou Mars dista et temps requis(s)).
Auteur : Marie-Helene Gadbois Del Carpio
Date de derniere modification : 14 septembre 2019 (derniere version)
Date de creation : 12 septembre 2019
Version 1.1 : 14 septembre 2019, modification legere et ajout de commentaires, Marie-Helene Gadbois Del Carpio
Entrees :
 (clavier) angle de tir (reel positif)
 (clavier) vitesse initiale (reel positif)
 (clavier) attraction gravitationnelle (reel positif non nul)
 Sorties :
 altitude maximale (reel positif)
 (clavier) distance parcourue (reel positif)
 (clavier) temps requis (reel positif)
*/

// Indique qu�on veut utiliser la biblioth�que permettant d'ecrire et de lire
#include <iostream>
#include <cmath>

using namespace std;

int main()
{
	//Etablir les constantes
	const double MI_HR_EN_M_SEC = 0.44704;
	const double DEG_EN_RAD = 0.0174533;
	const double KM_EN_M = 1000;
	
	//Mettre les entrees
	cout << "Entrez l'angle de tir en degres:" << endl;
	     double angleDeTirDeg; //en degres
    cin >> angleDeTirDeg;
	cout << "Entrez la vitesse initiale en milles par heure:" << endl;
	    double vitesseInitialeMiHr; //en milles par heure
	cin >> vitesseInitialeMiHr;
	cout << "Entrez l attraction gravitationnelle en metres par sec. au carre :" << endl; 
	    double attractionGravitationnelle; 
	cin >> attractionGravitationnelle;
	cout << endl;
	
	//Faire conversions des constantes
	double vitesseInitMSec = vitesseInitialeMiHr * MI_HR_EN_M_SEC; // milles par heure en metre par seconde
	double angleDeTirRad = angleDeTirDeg * DEG_EN_RAD; //degres en radian
	
	//Calculer les valeurs intermediaires
	double vitesseVerticale = sin(angleDeTirRad) * vitesseInitMSec;
	double vitesseHorizontale = cos(angleDeTirRad) * vitesseInitMSec;

	//Calculer les sorties
	double altMaxM = (vitesseVerticale * vitesseVerticale) / (2.0 * attractionGravitationnelle); //en metre
	double altMaxKm = altMaxM / KM_EN_M; // en kilometre
	double tempsRetour = (2.0 * vitesseVerticale) / attractionGravitationnelle; //en seconde
	double distanceParcourue = vitesseHorizontale * tempsRetour; //en metre

	//Afficher les resultats
	cout << "L'altitude maximale atteinte par le projectile est de " << altMaxKm << " km" << endl;
	cout << "La distance horizontale parcourue par le projectile est de " << distanceParcourue << " m" << endl;
	cout << "Le temps requis pour que le projectile touche le sol est de " << tempsRetour << " s" << endl;
	
	return 0;
}
